#!/bin/env python3
import logging
import argparse
import urllib
import json
import os
import datetime
import requests


def environ_or_required(key, help_text=""):
    return ({
        'default': os.environ.get(key)
    } if os.environ.get(key) else {
        'required': True
    } | {
        'help':
        help_text + "Can also be specified using the env var " + key
    })


def post_gitlab_wikipage(gitlab, token, gitlab_project, prefix, filename,
                         content):
    if prefix:
        # filename = urllib.parse.quote(prefix + "/" + filename, safe="")
        filename = prefix + "/" + filename

    gitlab_project_encoded = urllib.parse.quote(gitlab_project, safe="")
    headers = {'PRIVATE-TOKEN': token}
    data = {'format': "markdown", "title": filename, "content": content}

    post = requests.post(
        f"{gitlab}/api/v4/projects/{gitlab_project_encoded}/wikis",
        headers=headers,
        data=data)

    if post.status_code != 200:
        logging.error("Error when creating Wikipage")


def get_hedgedoc_content(url, token):
    cookies = {'connect.sid': token}
    content = requests.get(url + "/download", cookies=cookies)

    if content.status_code != 200:
        logging.error("Error when getting content")

    return content.content.decode(encoding="UTF-8")


def get_hedgedoc_last_edited_date(url, token):
    cookies = {'connect.sid': token}
    info_request = requests.get(url + "/info", cookies=cookies)
    info = json.loads(info_request.content.decode(encoding="UTF-8"))

    if info_request.status_code != 200:
        logging.error("Error when getting info")

    return datetime.datetime.fromisoformat(info["updatetime"])


def main():
    parser = argparse.ArgumentParser(prog='hedgedoc2gitlabwiki.py',
                                     description='Moves notes from hedgedoc to gitlab',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('hedgedoc_url',
                        help="URL of the HedgeDoc File")  # positional argument
    parser.add_argument('--gitlab',
                        dest="gitlab",
                        help="target gitlab",
                        default="https://gitlab.archlinux.org/")
    parser.add_argument('--project',
                        dest="gitlab_project",
                        help="target project on gitlab",
                        default="archlinux/infrastructure")
    parser.add_argument('--wiki-prefix',
                        dest="gitlab_wiki_prefix",
                        help="prefix for the wiki pages",
                        default="meetings")
    parser.add_argument('--gitlab-token',
                        dest="gitlab_token",
                        **environ_or_required('HEDGEDOC2GITLAB_GITLAB_TOKEN'))
    parser.add_argument(
        '--hedgedoc-token',
        dest="hedgedoc_token",
        **environ_or_required('HEDGEDOC2GITLAB_HEDGEDOC_TOKEN'))
    args = parser.parse_args()

    content = get_hedgedoc_content(args.hedgedoc_url, args.hedgedoc_token)
    last_edited = get_hedgedoc_last_edited_date(args.hedgedoc_url,
                                                args.hedgedoc_token)
    filename = last_edited.strftime("%Y-%m-%d")

    post_gitlab_wikipage(args.gitlab, args.gitlab_token, args.gitlab_project,
                         args.gitlab_wiki_prefix, filename, content)
    print(
        f"{args.gitlab}{args.gitlab_project}/-/wikis/{args.gitlab_wiki_prefix+'/' if args.gitlab_wiki_prefix else ''}{filename}"
    )


if __name__ == "__main__":
    main()
