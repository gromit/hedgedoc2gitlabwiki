# hedgedoc2gitlabwiki

To archive a devops meeting note:
```
# Set HEDGEDOC2GITLAB_GITLAB_TOKEN="..." and HEDGEDOC2GITLAB_HEDGEDOC_TOKEN="..."
$ ./hedgedoc2gitlabwiki.py "https://md.archlinux.org/devops-agenda-27-july"
```

Alias to always get env vars from passwordmanager:
```
alias hedgedoc2gitlabwiki='HEDGEDOC2GITLAB_GITLAB_TOKEN="$(passcommand)" HEDGEDOC2GITLAB_HEDGEDOC_TOKEN="$(passcommand)" hedgedoc2gitlabwiki'
```

## Documentation

```
$ hedgedoc2gitlabwiki.py -h

usage: hedgedoc2gitlabwiki.py [-h] [--gitlab GITLAB] [--project GITLAB_PROJECT] [--wiki-prefix GITLAB_WIKI_PREFIX] --gitlab-token GITLAB_TOKEN --hedgedoc-token HEDGEDOC_TOKEN hedgedoc_url

Moves notes from hedgedoc to gitlab

positional arguments:
  hedgedoc_url          URL of the HedgeDoc File

options:
  -h, --help            show this help message and exit
  --gitlab GITLAB       target gitlab (default: https://gitlab.archlinux.org/)
  --project GITLAB_PROJECT
                        target project on gitlab (default: archlinux/infrastructure)
  --wiki-prefix GITLAB_WIKI_PREFIX
                        prefix for the wiki pages (default: meetings)
  --gitlab-token GITLAB_TOKEN
                        Can also be specified using the env var HEDGEDOC2GITLAB_GITLAB_TOKEN (default: None)
  --hedgedoc-token HEDGEDOC_TOKEN
                        Can also be specified using the env var HEDGEDOC2GITLAB_HEDGEDOC_TOKEN (default: None)
```
